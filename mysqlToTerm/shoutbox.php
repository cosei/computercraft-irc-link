<?php 
date_default_timezone_set('Europe/Amsterdam');  # Set the default timezone and shizzle

$db = new mysqli('host', 'username', 'pass', 'database'); # Prepare the mysql connection

if($db->connect_errno > 0){ # Try to connect to the database
	die('Unable to connect to database [' . $db->connect_errno . ']'); # Display an error if the connection fails
}

$sql = $db->prepare("SELECT `log_time`, `member_name`, `body` FROM `smf_sp_shouts` WHERE id_shoutbox = ? ORDER BY `log_time` DESC LIMIT 30"); #Prepare the query
$shoutboxid = mysqli_escape_string($db, $_GET['id']); # Read and escape the input from _GET
$sql->bind_param('s', $shoutboxid); # Insert the result from last line into the querry

$sql->execute(); # Run the querry

$sql->bind_result($logtime, $member_name, $body); # Bind the query results 

$shouts = array(); # Prepare an empty array for the shouts

while($sql->fetch()) {
	array_push($shouts, array(date("m-d H:i", $logtime), $member_name , html_entity_decode($body)));
}

$json_shouts = json_encode($shouts);
print($json_shouts)

?>