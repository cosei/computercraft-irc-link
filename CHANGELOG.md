CHANGELOG

v0.1	Initial release to git
v0.2	- SSL support by default for Lurkbot
	- Small bugs fixed in Lurkbot
v0.3	- Updated README
	- Added database support; all visible chat on irc will now be put in database
	- Added database connection class
	- Added seperate database config file
	- Fixed typo in error msg
	- Added LICENSE
v0.3.1  Several bugs in v0.3 fixed
v0.3.2  Bugs in v0.3 and v0.3.1 fixed. Lurkbot is now logging all messages to the database
v0.3.3  Column added in table for channel; RplParser changed to support extra field
