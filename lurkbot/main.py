#================================================================================#
#										 #
#--------------------------------------------------------------------------------#
# This program is free software; you can redistribute it and/or			 #
# modify it under the terms of the GNU General Public License 			 #
# as published by the Free Software Foundation; either version 2		 #
# of the License, or (at your option) any later version. 			 #
# 										 #
# This program is distributed in the hope that it will be useful,		 #
# but WITHOUT ANY WARRANTY; without even the implied warranty of 		 #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 		 	 #
# GNU General Public License for more details. 					 #
# 										 #
# You should have received a copy of the GNU General Public License		 #
# along with this program; if not, write to the Free Software 			 #
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. #
#================================================================================#

__author__ = 'Boisei0'

import socket
import time
import os
from Connection import *
from Logger import *

class IRCBot():
	def __init__(self):
		self.config = {
			#server info
		        'host' : 'arethusa.tweakers.net',
		        'port' : 6697,
		        'channel' : '#minecraft.ftb',
			#client info
			'nick' : 'Lurkbot_',
		        'username' : 'lurkbot',
		        'hostname' : 'localhost',
		        'servername' : 'localhost',
		        'realname' : 'IRC bot written in Python',
			'nservpass' : '',
			#botconfig
                        'sleeptime' : 30,
                        'linebreak' : 5
		}
		if not os.path.exists('logs/'):
			print("Log folder doesn't exist yet! Creating...")
			os.makedirs('logs/')
		self.logObj = Logger('%s-%s' % (self.config['nick'], time.time()), 'logs/')
		
	def main(self):
		#Start connection with Master Daemon
		#self.mdsock = socket.socket()
                #self.mdsock.connect((config['mdhost'], config['mdport']))
                #print self.mdsock.recv(1024)

		self.logObj.append('Connecting to %(host)s:%(port)s...' % self.config)
		print 'Connecting to %(host)s:%(port)s...' % self.config
		try:
			con = Connection(self.config['host'], self.config['port'], self.config['nick'], self.config['username'], self.config['channel'], self.config['nservpass'], self.logObj)
			con.connect()
		except socket.error as ex:
        		print 'Error connecting to server %(host)s:%(port)s' % self.config
        		print 'Error: ' + str(ex)
  			sys.exit(1)

if __name__ == '__main__':
	ircbot = IRCBot()
	ircbot.main()
