#/!usr/bin/env python
# -*- coding: utf-8 -*-

#================================================================================#
#                                                                                #
#--------------------------------------------------------------------------------#
# This program is free software; you can redistribute it and/or                  #
# modify it under the terms of the GNU General Public License                    #
# as published by the Free Software Foundation; either version 2                 #
# of the License, or (at your option) any later version.                         #
#                                                                                #
# This program is distributed in the hope that it will be useful,                #
# but WITHOUT ANY WARRANTY; without even the implied warranty of                 #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                   #
# GNU General Public License for more details.                                   #
#                                                                                #
# You should have received a copy of the GNU General Public License              #
# along with this program; if not, write to the Free Software                    #
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. #
#================================================================================#

import os, time

class Logger:
	def __init__(self, logname, path):
		self.log = ''
		self.path = path + logname
		self.saveLog()

	def append(self, line):
		timestamp = time.strftime('%H:%M:%S ', time.gmtime(time.time()))
		#TODO: add utf-8 support
		try:
			self.log += str(timestamp + line + '\n')
		except UnicodeEncodeError as ex:
			print 'Error on append: %s' % str(ex)
		self.saveLog()

	def saveLog(self):
		logfile = open(self.path, "w")
		try:
			logfile.write(self.log)
		except IOError as ex:
			print 'Error while writing log %s; error: %s' (self.path, ex)
		finally:
			logfile.close()
