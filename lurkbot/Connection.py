#================================================================================#
#                                                                                #
#--------------------------------------------------------------------------------#
# This program is free software; you can redistribute it and/or                  #
# modify it under the terms of the GNU General Public License                    #
# as published by the Free Software Foundation; either version 2                 #
# of the License, or (at your option) any later version.                         #
#                                                                                #
# This program is distributed in the hope that it will be useful,                #
# but WITHOUT ANY WARRANTY; without even the implied warranty of                 #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                   #
# GNU General Public License for more details.                                   #
#                                                                                #
# You should have received a copy of the GNU General Public License              #
# along with this program; if not, write to the Free Software                    #
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. #
#================================================================================#

__author__ = 'Boisei0'

import re, socket, sys, time, ssl
from RplParser import *

class Connection:
	def __init__(self, host, port, nick, user, cojoin, nservpass, logObj):
		self.crlf = '\r\n'
		self.host = host
		self.port = port
		self.nick = nick
		self.user = user
		self.defChannel = cojoin # Channel to join at startup
		self.passwd = nservpass
		self.logObj = logObj
		self.privmsgregex = re.compile(r':(?P<nick>\S+)!(?P<user>\S+)@(?P<host>\S+) PRIVMSG (?P<channel>\S+) :(?P<msg>.*)')
		self.inviteregex = re.compile(r':(?P<nick>\S+)!(?P<user>\S+)@(?P<host>\S+) INVITE (?P<botname>\S+) :(?P<channel>.*)')
		self.pingregex = re.compile(r'PING :(?P<pingquery>\S+)')
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	def handshake(self):
		self.socket.send('NICK ' + self.nick + self.crlf)
		self.socket.send('USER ' + self.user + ' 0 * :' + self.user + self.crlf)
		if len(self.passwd) > 0:
			self.socket.send('PRIVMSG NickServ :IDENTIFY ' + self.passwd + self.crlf)

	def join(self, channel):
		self.logObj.append('Joining %s' % channel)
		self.socket.send('JOIN %s%s' % (channel, self.crlf))

	def connect(self):
		self.socket.connect((self.host, self.port))
		self.socket = ssl.wrap_socket(self.socket) #potential ssl fix
		self.handshake()
		self.join(self.defChannel)
		while True:
			rpl = self.socket.recv(1024)
			parser = RplParser(self.socket, self.nick, self.logObj)
			parser.incParse(rpl)
