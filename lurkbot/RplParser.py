#/!usr/bin/env python
# -*- coding: utf-8 -*-

#================================================================================#
#                                                                                #
#--------------------------------------------------------------------------------#
# This program is free software; you can redistribute it and/or                  #
# modify it under the terms of the GNU General Public License                    #
# as published by the Free Software Foundation; either version 2                 #
# of the License, or (at your option) any later version.                         #
#                                                                                #
# This program is distributed in the hope that it will be useful,                #
# but WITHOUT ANY WARRANTY; without even the implied warranty of                 #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                   #
# GNU General Public License for more details.                                   #
#                                                                                #
# You should have received a copy of the GNU General Public License              #
# along with this program; if not, write to the Free Software                    #
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. #
#================================================================================#

__author__ = 'Boisei0'

import re, sys, time
from Logger import *
from dbConnection import *

class RplParser:
	def __init__(self, socket, botname, logObj):
		self.crlf = '\r\n'
		self.botname = botname
		self.logObj = logObj
		self.statusmotd = [375, 372, 376]
		self.serverregex = re.compile(r':(?P<serverID>\S+) (?P<status>\S+) (?P<botnick>\S+) :(?P<msg>.*)')
		self.privmsgregex = re.compile(r':(?P<nick>\S+)!(?P<user>\S+)@(?P<host>\S+) PRIVMSG (?P<channel>\S+) :(?P<msg>.*)')
                self.inviteregex = re.compile(r':(?P<nick>\S+)!(?P<user>\S+)@(?P<host>\S+) INVITE (?P<botname>\S+) :(?P<channel>.*)')
                self.pingregex = re.compile(r'PING :(?P<pingrpl>.+)')
		self.socket = socket
		self.cmdhandler = CommandHandler(self.socket, self.logObj)
		
	def incParse(self, rpl):
		if rpl.find('PRIVMSG') != -1:
			self.chkPrivmsg(rpl)
		elif rpl.find('INVITE') != -1:
			self.chkInvite(rpl)
		elif rpl.find('PING') != -1:
			self.chkPing(rpl)
		elif self.chkServermsg(rpl) == False:
			self.logObj.append(rpl)

	def chkServermsg(self, rpl):
		servermsg = self.serverregex.search(rpl)
		if servermsg:
			servermsgDict = servermsg.groupdict()
			if (servermsgDict['botnick'] == self.botname) and (servermsgDict['status'] in self.statusmotd):
				pass
		else:
			return False

	def chkInvite(self, rpl):
		invitemsg = self.inviteregex.search(rpl)
		if invitemsg:
			invitemsgDict = invitemsg.groupdict()
			if invitemsgDict:
				self.logObj.append('Invited to channel %s by %s' % (invitemsgDict['channel'], invitemsgDict['nick']))
				self.cmdhandler.join(invitemsgDict['channel'])

	def chkPing(self, rpl):
		pingmsg = self.pingregex.search(rpl)
		if pingmsg:
			pingmsgDict = pingmsg.groupdict()
			if pingmsgDict:
				self.logObj.append('Ping detected: %s' % rpl)
				self.cmdhandler.pong(pingmsgDict['pingrpl'])

	def chkPrivmsg(self, rpl):
		privmsg = self.privmsgregex.search(rpl)
		if privmsg:
			privmsgDict = privmsg.groupdict()
			if privmsgDict:
				self.logObj.append('%s: <%s> %s' % (privmsgDict['channel'], privmsgDict['nick'], privmsgDict['msg']))

				dbObj = DatabaseConnection()
				dbObj.openCon()
				msg = privmsgDict['msg'].strip(' \t\n\r')
				query = 'INSERT INTO irclog(timestamp, username, msg, channel) VALUES ("%s", "%s", "%s", "%s")' % (dbObj.prepareData(time.time()), dbObj.prepareData(privmsgDict['nick']), dbObj.prepareData(msg), dbObj.prepareData(privmsgDict['channel']))
				dbObj.execInsQuery(query)
				dbObj.closeCon()
				
class CommandHandler:
	def __init__(self, socket, logObj):
		self.crlf = '\r\n'
		self.socket = socket
		self.logObj = logObj

	def join(self, channel):
		self.socket.send('JOIN %s%s' % (channel, self.crlf))

	def say(self, channel, msg):
		reply = 'PRIVMSG %s :%s%s' % (channel, msg, self.crlf)
		try:
			self.logObj.append(reply)
			self.socket.send(reply)
		except Exception as ex:
			print 'Error on CommandHandler.say: %s' % str(ex)
			try:
				self.say(channel, 'Error on CommandHandler.say: %s' % str(ex)) #Fugly, error attracting, recursive code
			except Exception as exRec:
				print 'Error on Error on Commandhandler.say: %s' % str(exRec)

	def pong(self, pongmsg):
		self.socket.send('PONG %s%s' % (pongmsg, self.crlf))

	def quit(self, quitmsg=''):
		self.socket.send('QUIT :%s%s' % (quitmsg, self.crlf))
		sys.exit(1)
