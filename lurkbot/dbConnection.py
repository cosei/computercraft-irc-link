#/!usr/bin/env python
# -*- coding: utf-8 -*-

#================================================================================#
#                                                                                #
#--------------------------------------------------------------------------------#
# This program is free software; you can redistribute it and/or                  #
# modify it under the terms of the GNU General Public License                    #
# as published by the Free Software Foundation; either version 2                 #
# of the License, or (at your option) any later version.                         #
#                                                                                #
# This program is distributed in the hope that it will be useful,                #
# but WITHOUT ANY WARRANTY; without even the implied warranty of                 #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                   #
# GNU General Public License for more details.                                   #
#                                                                                #
# You should have received a copy of the GNU General Public License              #
# along with this program; if not, write to the Free Software                    #
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. #
#================================================================================#

import MySQLdb as mdb
from dbConfig import *

class DatabaseConnection:
	def __init__(self):
		self.conn = None

	def openCon(self):
		try:
			self.conn = mdb.connect(MDB_HOST, MDB_USER, MDB_PASS, MDB_DB)
			self.cursor = self.conn.cursor()

		except mdb.Error as ex:
			print 'Error: %s' % str(ex)
	
	def closeCon(self):
		try:
			if self.conn:
				self.conn.close()
			if self.cursor:
				self.cursor.close()

		except mdb.Error as ex:
			print 'Error: %s' % str(ex)

	def execInsQuery(self, query): # Insert queries don't need data fetched
		try:
			self.cursor.execute(query)
			self.conn.commit()

		except mdb.Error as ex:
			print 'Error: %s' % str(ex)

	def exexSelQuery(self, query): # Select queries need data fetched
		try:
			self.cursor.execute(query)
			self.conn.commit()
			result = self.cursor.fetchAll()
			return result

		except mdb.Error as ex:
			print 'Error: %s' % str(ex)

	def prepareData(self, rawData): # Prepare data for database
		try:
			return mdb.escape_string(str(rawData))

		except mdb.Error as ex:
			print 'Error: %s' % str(ex)
